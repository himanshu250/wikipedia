
import UIKit

protocol TextPresentable {
    var searchText: DynamicTypes<String> { get }
    func updateSearchText(text: String)
    var textColor: UIColor { get }
    var font: UIFont { get }
}

extension TextPresentable {
  
  var textColor: UIColor {
    return .red
  }
  
  var font: UIFont {
    return .systemFont(ofSize: 17)
  }
  
}
