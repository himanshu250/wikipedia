//
//  DynamicTypes.swift
//  Wikipedia
//
//  Created by Himanshu on 21/07/18.
//  Copyright © 2018 Wikipedia. All rights reserved.
//

import Foundation

class DynamicTypes<T> {
    
    typealias  Listener = (T) -> Void
    var listener: Listener?
    var bind :(T) -> () = { _ in }
    var value :T? {
        didSet {
            listener?(value!)
        }
    }
    init(_ val :T) {
        value = val
    }
    func bind(listenr: Listener?) {
        self.listener = listenr
        listener?(value!)
    }
}


