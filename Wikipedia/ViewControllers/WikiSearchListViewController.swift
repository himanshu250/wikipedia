//
//  WikiSearchListViewController.swift
//  Wikipedia
//
//  Created by Himanshu on 20/07/18.
//  Copyright © 2018 Himanshu. All rights reserved.
//

import UIKit
import CoreData

protocol SearchListViewControllerDelegate: class {
    func didSelectOptionFromHistory(text: String, list: [String])
}

class WikiSearchListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var contentTable: UITableView!
    private var list = [String]()
    weak var delegate: SearchListViewControllerDelegate?
    
    static func viewController(with list: [String]) -> WikiSearchListViewController {
        let mainView = UIStoryboard(name: "Main", bundle: nil)
        let searchListVC = mainView.instantiateViewController(withIdentifier: "searchListVC") as! WikiSearchListViewController
        searchListVC.list = list
        searchListVC.list.reverse()
        return searchListVC
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WikiHistoryCell", for: indexPath) as! WikiHistoryCell
        cell.label.text = list[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            if list[indexPath.row] != WikiManager.sharedInstance.currentSearchedText {
               delegate?.didSelectOptionFromHistory(text: list[indexPath.row], list: list)
            }
            dismiss(animated: true, completion: nil)
    }
    
    
    
    
    @IBAction func deleteHistoryTapped() {
    }
    
    @IBAction func dismissButtonTapped() {
        dismiss(animated: true, completion: nil)
    }
}

