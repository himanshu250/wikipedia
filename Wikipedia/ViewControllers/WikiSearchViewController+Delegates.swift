//
//  WikiSearchViewController + Delegates.swift
//  Wikipedia
//
//  Created by Himanshu on 20/07/18.
//  Copyright © 2018 Himanshu. All rights reserved.
//

import Foundation
import UIKit

extension WikiSearchViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return serachResultsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let wikiSearchCell = tableView.dequeueReusableCell(withIdentifier: "wikiSearchCell") as! WikiSearchCell
        if serachResultsArray.count > 0 {
            wikiSearchCell.entity = serachResultsArray[indexPath.row]
        }
        return wikiSearchCell
    }
    
    func insertMoreIndexPath(count: Int) {
        var paths = [IndexPath]()
        for _ in 0..<count {
            paths.append(IndexPath(row: serachResultsArray.count - 1, section: 0))
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if serachResultsArray.count > 0 {
            let actualTitle = serachResultsArray[indexPath.row].title!.replacingOccurrences(of: " ", with: "_")
            let url = "https://en.wikipedia.org/wiki/\(actualTitle)"
            delegate1?.updateUrl(url: url)
            delegate1?.updateTitle(title: actualTitle)
            let vc = WikipediaViewController.viewController(with: delegate1!)
            navigationController?.pushViewController(vc, animated: true)
        }
    }
}
