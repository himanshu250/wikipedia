//
//  WikiManager + Alamofire.swift
//  Wikipedia
//
//  Created by Himanshu on 20/07/18.
//  Copyright © 2018 Himanshu. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import CoreData

extension WikiManager {
    func request(url: String, type: HTTPMethod, with completion: @escaping WikiCompletionBlock) {
        Alamofire.request(url, method: type).responseJSON { response in
            response.result.ifSuccess {
                DispatchQueue.main.async {
                        do {
                            let responseResult = response.result
                            let resultsDictionary = responseResult.value as? [String: AnyObject]
                            guard let _ = resultsDictionary, let photosContainer = resultsDictionary!["query"] as? NSDictionary, let serachResultsArray = photosContainer["pages"] as? [NSDictionary] else { return }
                            self.saveInCoreDataWith(array: serachResultsArray as! [[String : AnyObject]])
                            let WikiPhotoArray: [Wiki] = serachResultsArray.map { photoDictionary in
                                let WikiPhoto = self.createUserEntityFrom(dictionary: photoDictionary as! [String : AnyObject])
                                return WikiPhoto as! Wiki
                            }
                            completion(nil, WikiPhotoArray)
                        } catch let error as NSError {
                            completion(error as NSError?, nil)
                        }
                }
            }
            response.result.ifFailure {
                DispatchQueue.main.async {
                    let code = response.response?.statusCode ?? 0
                    completion(nil, nil)
                }
            }
        }
    }
    
    
    private func createUserEntityFrom(dictionary: [String: AnyObject]) -> NSManagedObject? {
        let context = CoreDataClient.sharedInstance.context
        if let photoEntity = NSEntityDescription.insertNewObject(forEntityName: "Wiki", into: context) as? Wiki {
            photoEntity.title = dictionary["title"] as! String
            photoEntity.url = dictionary["thumbnail"]?["source"] as? String
            if let description = dictionary["terms"]?["description"] as? [String] {
               photoEntity.entityDescription = description[0]
            }
            return photoEntity
        }
        return nil
    }
    
    private func saveInCoreDataWith(array: [[String: AnyObject]]) {
        _ = array.map{self.createUserEntityFrom(dictionary: $0)}
        do {
            try CoreDataClient.sharedInstance.context.save()
        } catch let error {
            print(error)
        }
    }
}
