//
//  UIImageView+download.swift
//  WikiPedia
//
//  Created by Himanshu on 20/07/18.
//  Copyright © 2018 Himanshu. All rights reserved.
//

import  UIKit

extension UIImageView {
    
    func downloadedFrom(url: URL, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        contentMode = mode
        self.image = UIImage(named: "placeholder")
            ImageDownloader.sharedInstance.downloadedFrom(url: url) { (image) in
                WikiManager.imageURLDict[url.absoluteString] = image
                DispatchQueue.main.async {
                    self.image = image
                }
       
    }
}
}
