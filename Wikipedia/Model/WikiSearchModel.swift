//
//  WikiSearchModel.swift
//  Wikipedia
//
//  Created by Himanshu on 21/07/18.
//  Copyright © 2018 Wikipedia. All rights reserved.
//

import Foundation

class WikiSearchModel: NSObject {
    var searchText = ""
    var searchUrl = ""
    var title = ""
}
