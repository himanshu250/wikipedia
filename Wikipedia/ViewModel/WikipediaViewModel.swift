//
//  WikiPediaViewModel.swift
//  Wikipedia
//
//  Created by Himanshu on 21/07/18.
//  Copyright © 2018 Wikipedia. All rights reserved.
//

import Foundation
import UIKit


class WikiPediaViewModel: NSObject, WikiPresentable {
    var searchModel = WikiSearchModel()
    var url: DynamicTypes<String> { return DynamicTypes<String>(searchModel.searchUrl) }
    var title: DynamicTypes<String> { return DynamicTypes<String>(searchModel.title) }
    
    func updateUrl(url: String) {
        searchModel.searchUrl = url
    }
    
    func updateTitle(title: String) {
        searchModel.title = title
    }
    
}
