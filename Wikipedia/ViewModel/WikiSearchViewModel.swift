//
//  EmployeeIListViewModel.swift
//  NoonAcademy
//
//  Created by Himanshu on 21/07/18.
//  Copyright © 2018 NoonAcademy. All rights reserved.
//

import Foundation
import UIKit


class WikiSearchViewModel: NSObject, TextPresentable {
    var searchModel =  WikiSearchModel()
    var searchText: DynamicTypes<String> { return DynamicTypes<String>(searchModel.searchText) }
    func updateSearchText(text: String) {
        searchModel.searchText = text
    }
   
}
